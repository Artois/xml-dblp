# Projet XML : TP SAX

## Arthur Brandao & Maxence Bacquet - M2 ILI

Programme pour interroger la base de données DBLP au format XML.

## Fichier XML Sources

Le fichier de base de données DBLP au format XML est disponible sur les liens suivants
 - https://dblp.uni-trier.de/xml/
 - https://dblp.org/faq/How+can+I+download+the+whole+dblp+dataset.html

## Documentation des commandes

La documentation suivante pars du principe qu'un terminal est ouvert dans le dossier du projet et que nodejs est installée (version >= 10.12.0, le projet a été testé avec la version 12.13.0). 

Pour installer les dépendances il suffit d'utiliser la commande suivante dans le repertoire du projet :

```bash
npm i
```

## Partie 0 - Paramétrage et Déploiement

Le projet est composé de plusieurs programmes avec la même base. Pour exécuter les scripts propres à chaque programme il faut utiliser la commande npm suivante :

```bash
npm start <program> [script]
# Par exemple
npm start extract
npm start coauth test
```

Ou directement avec Node Js : 

```bash
node main.js <program> [script]
# Par exemple
node main.js extract
node main.js coauth test
```

Si aucun script n'est spécifié le programme est appelé directement sans argument.

Pour séparer chaque programme en un ensemble de fichier indépendant il faut lancer la commande :

```bash
npm run deploy
```

Un dossier sera créé avec le nom de chaque programme dans le dossier `dist` (si il n'existe pas il sera créé).

Chaque dossier comportera ses sources, son package.json et son README.md.

Le paramétrage des différents projets (les scripts, la version, les dépendances, ...) sont paramétrables depuis le fichier deploy.json

L'utilisation des commandes avec npm génère automatiquement des informations dans la sortie standard, comme ci-dessous :

```bash
> projet-dblp@2.0.1 start /home/arthur/TP/M2-S1/xml/projet-dblp
> node main.js
```

Pour éviter l'affichage de ces informations dans la console il est possible d'utiliser l'option `silent` juste après la commande npm (voir les exemples ci-dessous).

```bash
npm --silent start
npm --silent run test
```

## Partie 1 - Recherche des co-auteurs d'un auteur

L'objectif du programme est de trouver les co-auteurs d'un auteur donné. Le programme possède deux fonctions, la première permet d'effectuer la recherche depuis le fichier xml ou depuis le fichier obtenu suite au pré-traitement, la seconde permet de faire un pré-traitement de la base pour créer un fichier .ppf (Pre Processing File).

### Commande de recherche des co-auteurs sans pré-traitement

La commande de base pour la recherche des co-auteurs sans pré-traitement est la suivante :

```bash
node coauth.js -p <source>
```

L'option `-p <source>` ou `--process <source>`permet d'effectuer une recherche des co-auteurs sans faire d'abord un pré-traitement. `<source>` correspond au chemin vers le fichier de base de données en XML. 

Il est possible d'utiliser l'option `-n <string>` ou `--name <string>` ou `--name=<string>` pour indiquer le nom de l'auteur pour la recherche. Si l'option est absente le nom est demandé dans la console.

Voici quelques exemples expliqués de la commande avec l'option process :

```bash
# Lance le pré-traitement sur le fichier ./data/dblp.xml, génere le fichier ./data/data.ppf et le nom de l'auteur pour la recherche est demandé dans la console
node coauth.js --process ./data/dblp.xml

# Lance le pré-traitement sur le fichier ./data/dblp.xml, génere le fichier ./data/data.ppf et fait la recherche sur Fabien Delorme
node coauth.js -p ./data/dblp.xml -n "Fabien Delorme"
```

### Commande de pré-traitement de la base

Cette commande permet de générer un fichier .ppf (si aucune destination n'est indiquée le chemin vers le fichier seras le suivant : `./data/data.ppf`)

La commande est :

```bash
node coauth.js prepare <source> [destination]
```

- `<source>` : correspond au chemin vers la base de données au format xml
- `[destination]` : Le chemin et le nom du fichier .ppf à créer suite au pré-traitement de la base de données (si l'extension n'est pas .pff elle seras automatiquement ajoutée), cet argument est optionel

### Commande de recherche des co-auteurs

La commande de base pour la recherche des co-autheurs est la suivante :

```bash
node coauth.js
```

Avec cette commande le programme considère que le fichier .ppf est dans `./data/data.ppf`. Le nom de l'auteur dont il faut chercher les co-auteurs seras demandé par le programme dans la console.

La commande admet plusieurs options :

- `-n <string>` ou `--name <string>` ou `--name=<string>` : Pour indiquer le nom de l'auteur pour la recherche
- `-f <source>` ou `--file <source>` ou `--file=<source>` : Pour indiquer le chemin vers le fichier .ppf à utiliser
- `-v` ou `--verbose` : Permet d'afficher plus d'informations dans la console
- `-h` ou `--help` : Affiche l'aide
- `-V` ou `--version` : Affiche la version du programme

Voici quelques exemples avec des options :

```bash
node coauth.js -n "Fabien Delorme"
node coauth.js --name="Anne-Cécile Caron" -f "./data/data.ppf"
node coauth.js --name "Francis Bossut" --file "./tmp/coauth.ppf" -v
```

Le retour du programme sans l'option verbose est similaire au retour du programme de reference fournit. Attention la recherche sur le nom est sensible à la casse.

### Temps d'exécution

Les temps d'executions des commandes sur nos PC sont de :

|                             | Ubuntu (I7-8550U) | Kubuntu (I5-7300HQ) | Kde Neon (I7-8550U) |
| --------------------------- | :-------------: | :-----------------: | :-----------------: |
| **Référence (Total)**       | ~ 4 minutes 20 |   ~ 2 minutes 30    |   ~ 2 minutes 30    |
| **Préparation**             | ~ 3 minutes 20 |     ~ 2 minutes     |    ~ 1 minute 45    |
| **Recherche**               | 4 à 6 secondes |   5 à 10 secondes   |   3 à 5 secondes    |

## Partie 2 - Extraction de données d'un auteur

L'objectif du programme est de générer un document XML avec les infos extraites des articles et des inproceedings d'un auteur donné dans la base de données DBLP au format XML. Le document comporte aussi une section avec tous les co-auteurs de l'auteur donné dans les données extraites.

### Commande d'extraction des <u>données</u>

La commande pour extraire les données de la base est la suivante : 

```bash
node extract.js
```

Par défaut le programme va chercher le fichier XML de la base dans le dossier data avec le nom dblp.xml et créer un fichier extract.xml dans ce même dossier. Le nom de l'auteur sera demandé dans la console avant de lancer la recherche.

Le programme possède plusieurs options :

- `-n <string>` ou `--name <string>` ou `--name=<string>` : Pour indiquer le nom de l'auteur pour la recherche
- `-f <source>` ou `--file <string>` ou `--file=<string>` : Pour indiquer le chemin vers le fichier de la base (en XML)
- `-o <source>` ou `--output <string>` ou `--output=<string>` : Pour indiquer le chemin pour la création du fichier contenant les données de l'extraction (en XML)
- `-v` ou `--verbose` : Permet d'afficher plus d'informations dans la console
- `-h` ou `--help` : Affiche l'aide
- `-V` ou `--version` : Affiche la version du programme

Voici quelques exemples et leur explication de la commande avec ces options :

```bash
# Recherche les informations sur Fabien Delorme dans la base ./data/dblp.xml et extrait les informations dans ./data/extract.xml
node extract.js --name "Fabien Delorme"

# Recherche les informations sur Fabien Delorme dans la base ./data/dblp.xml et extrait les informations dans ./data/fabienDelorme.xml
node extract.js --name="Fabien Delorme" --output "./data/fabienDelorme.xml"

# Recherche les informations sur Fabien Delorme dans la base ./data/base.xml et extrait les informations dans ./data/extract.xml
node extract.js -n "Fabien Delorme" -o "./data/fabienDelorme.xml" -f "./data/base.xml"

# Recherche les informations sur Fabien Delorme dans la base ./data/base.xml et extrait les informations dans ./data/extract.xml et affiche des informations sur l'exécution
node extract.js -n "Fabien Delorme" -o "./data/fabienDelorme.xml" -f "./data/base.xml" -v
```

### Temps d'exécution

|                | Ubuntu (I7-8550U) | Kubuntu (I5-7300HQ) | KDE Neon (I7-8550U) |
| -------------- | :-------------: | :-----------------: | :-----------------: |
| **Extraction** | ~  1 minute 20 |  50 à 70 secondes   |  45 à 50 secondes   |

## Partie 3 - Transformation en HTML

Pour convertir le document xml extrait en un fichier html il suffit d'utiliser la commande suivante (ou son équivalent avec npm) :

```bash
xsltproc -o out.html convert.xsl ./data/extract.xml
# <=>
npm run xslt
```

La fichier est crée dans le dossier data sous le nom de `extract.html` en utilisant la commande npm