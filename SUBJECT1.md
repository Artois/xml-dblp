# TP SAX
Vous devez réaliser un programme permettant d'interroge la base de données DBLP représentée au format XML.
Le choix du langage de programmation et des technologies utilisés sont libres. La qualité du code source ne sera pas évaluée, seul le résultat le sera.
Votre programme doit effectuer les tâches suivantes:

## 1. Connaître tous les co-auteurs d'un auteur donné

Dans le fichier dblp.xml, il y a (notamment) des éléments "inproceedings" et des éléments "article" représentant des articles scientifiques,
tous écrits par un ou plusieurs auteurs (référencés dans des éléments "authors").
Le but de votre programme est d'indiquer, pour un nom d'auteur donné en entrée, la liste de tous ses co-auteurs, c'est-à-dire de tous les auteurs
ayant écrit au moins un article avec lui ou elle.
Par exemple, le programme fourni par l'enseignant s'utilisera de la manière suivante:

    $> ./dblp-prof -name="Fabien Delorme" dblp.xml
    "Fabien Delorme" has 7 coauthors:
    - Denis Stackowiak
    - Jean-Pierre Pécuchet
    - Jérôme Lehuen
    - Nathalie Chetcuti-Sperandio
    - Nicolas Delestre
    - Stéphane Cardon
    - Sylvain Lagrue

Notez que le programme met un certain temps à s'exécuter, le temps de parser le fichier XML.
Notez également que l'ordre dans lequel les noms s'affichent peuvent varier.
Votre programme devra générer exactement la même sortie que le programme de l'enseignant. N'hésitez pas à utiliser la commande `diff` pour comparer
votre sortie et celle du programme de référence:

    $> ./dblp-prof -name="Fabien Delorme" dblp.xml | sort >out-prof.txt
    $> ./mon-prog -name="Fabien Delorme" dblp.xml | sort >out-moi.txt
    $> diff out-prof.txt out-moi.txt

Si la commande `diff` n'affiche rien, les deux fichiers sont identiques: le résultat est donc correct.

Si vous trouvez une erreur dans le programme de l'enseignant (votre programme n'affiche pas la même chose et c'est vous qui avez raison), n'hésitez pas
à le signaler: vous aurez des points de bonus.

### Critères d'évaluation:
le programme sera testé avec 10 noms (choisis au moment de l'évaluation). Pour chaque nom, si votre programme produit la même sortie que le programme de référence, vous aurez
1 point, pour un total de 10 points au maximum. **Attention**: votre programme sera automatiquement "killé" s'il n'a pas produit de réponse après un temps limite, égal à trois fois le temps d'exécution du
programme de référence. Par exemple, si le programme de référence a donné une réponse au bout de 2 minutes 10, votre programme doit donner une réponse en 6 minutes 30 au maximum.

## 2. Découper le programme en 2 parties

Le programme écrit ci-dessus est trop lent: à chaque fois, la quasi-totalité du temps est passée à parser le fichier XML. Vous allez donc décomposer votre exécutable en deux exécutables:

- a: un programme qui parse le fichier XML et en extrait les informations pertinentes, pour les stocker dans un fichier externe.
- b: un programme qui, comme pour la question 1, donne la liste des coauteurs d'un auteur mais lit, non pas le fichier XML, mais le fichier généré lors de l'étape a.

En pratique, il peut s'agir du même exécutable qui exécute toutes les tâches (comme le fait le programme de l'enseignant), ou de trois exécutables différents, selon vos préférences.

Le fichier généré par le programme a peut être, au choix, un format binaire ou textuel de votre choix (il peut s'agir d'un document XML ou JSON, par exemple, ou du format de sérialisation de votre
langage de programmation), ou une base de données SQLite (attention, pas de MySQL, PostgreSQL ou autres).

### Critères d'évaluation:
La vitesse d'exécution du programme a est peu importante, puisqu'il ne sera lancé qu'une fois de temps en temps. Il est d'ailleurs préférable de passer du temps à générer ce fichier pour optimiser les requêtes
futures. Mais soyez tout de même raisonnable. Le programme a sera "killé" s'il met plus de dix fois plus de temps à s'exécuter que le programme de référence. Attention donc, si vous ne veillez pas à ce critère, vous aurez 0 sur toute cette partie.

Le programme b sera testé avec 10 noms (choisis au moment de l'évaluation). Pour chaque nom, si votre programme produit la même sortie que le programme de référence, vous aurez
1 point, pour un total de 10 points au maximum. **Attention**: votre programme sera automatiquement "killé" s'il n'a pas produit de réponse après un temps limite, égal à trois fois le temps d'exécution du
programme de référence.


## Détails pratiques
Ce travail est à réaliser en binôme ou seul.

**TRÈS IMPORTANT ! Un malus de 5 points sera automatiquement appliqué en cas de non-respect des règles suivantes.**

Vous devrez remettre à l'enseignant une archive **.zip** ou **.tar.gz** ou **.tgz** dont le nom sera les noms de familles dex deux membres du binôme, séparés par un tiret. Les
symbôles non alphabétiques seront supprimés, les accents et cédilles supprimés. Par exemple, si votre nom de famille est "de La Tour" et celui de votre binôme "Petit-Grégoire", votre fichier s'appellera
**delatour-petitgregoire.tgz**.  En cas de doute, demandez à l'enseignant.

L'archive doit contenir tous vos fichiers sources, ainsi qu'un fichier README indiquant :
- comment compiler vos programmes,
- comment les utiliser pour chacune des questions ci-dessus.

Le tout doit être envoyé par mail à l'adresse **fdelorme@pm.me** au plus tard la veille au soir de l'examen écrit.
