/**
 * Parseur d'attribut retourner par saxophone
 */

module.exports.parse = function (strAttr) {
    let result = {};
    strAttr.trim().split(' ').forEach(elt => {
        const split = elt.split('=');
        if (split[1] !== undefined) {
            result[split[0]] = split[1].trim().replace(/^"+|"+$/g, '');
        }
    });
    return result;
};