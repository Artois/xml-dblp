const timer = class Timer {

    constructor() {
        this.begin = 0;
        this.end = 0;
    }

    start() {
        this.begin = this._timestamp();
    }

    stop() {
        this.end = this._timestamp();
    }

    second() {
        if (this.begin === 0 || this.end === 0) {
            return 0;
        }
        return this.end - this.begin;
    }

    time() {
        const time = this.second();
        let result;
        if ((time / 60) >= 1) {
            const tmp = parseInt(time / 60);
            result = tmp + 'm ' + (time - (tmp * 60)) + 's';
        } else {
            result = time + 's';
        }
        return result;
    }

    _timestamp() {
        return parseInt(Date.now().toString().substr(0, 10));
    }

};

module.exports = new timer();
