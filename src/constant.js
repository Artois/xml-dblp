/**
 * Les constantes du projet
 */

const cst = {
    'SUCCESS': 0,
    'FILE_NOT_FOUND': 1,
    'PREPROCESS_NOT_FOUND': 2
};

const cstToStr = [
    'Success',
    'Unable to find the file',
    'Unable to find the pre-processing file'
];

module.exports = cst;

module.exports.toString = function (key) {
    return cstToStr[key];
};
