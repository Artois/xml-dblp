const parser = require('./parser').get();

module.exports.from = function (path) {
    return parser.from(path);
};

module.exports.setup = function (source, dest) {
    return parser.from(source).to(dest);
};

module.exports.get = function () {
    return parser;
};
