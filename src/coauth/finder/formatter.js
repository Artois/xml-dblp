const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();

const formatter = class Formatter {

    format(result) {
        let name = this._decode(result.name);
        let coAuthors = result.coAuthors.map(this._decode);
        coAuthors.sort(this._alphabeticalSort);
        let str = `"${name}" has ${coAuthors.length} coauthors:`;
        for (const index in coAuthors) {
            str += `\n- ${coAuthors[index]}`;
        }
        return str;
    }

    _decode(elt) {
        return entities.decode(elt);
    }

    _alphabeticalSort(a, b) {
        return (a > b) ? 1 : -1;
    }

};

module.exports = formatter;
