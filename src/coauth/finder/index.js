const finder = require('./finder').get();
let formatter = require('./formatter');
formatter = new formatter();

module.exports.in = function (path) {
    return finder.from(path);
};

module.exports.get = function () {
    return finder;
};

module.exports.result = finder.result;

module.exports.print = function (result) {
    result.setFormatter(formatter);
    return result.toString();
};