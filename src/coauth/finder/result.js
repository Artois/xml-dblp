const result = class Result {

    constructor() {
        this.name = '';
        this.coAuthors = [];
        this.formatter = null;
    }

    setFormatter(formatter) {
        if (typeof formatter.format === 'function') {
            this.formatter = formatter;
        }
    }

    removeFormatter() {
        this.formatter = null;
    }

    toString() {
        if (this.formatter !== null) {
            return this.formatter.format(this);
        }
        return JSON.stringify(this.coAuthors);
    }

};

module.exports = result;
