const LineReader = require('n-readlines');

module.exports.begin = function (file, search) {
    let stop = false;
    let count = 0;
    const liner = new LineReader(file);
    do {
        let line = '';
        if (!(line = liner.next())) {
            liner.close();
            return 0;
        }
        line = line.toString();
        if (line.includes(search)) {
            stop = true;
        } else {
            count++;
        }
        count += line.length;
    } while (!stop);
    liner.close();
    return count;
};