<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" indent="yes" version="5"/>

    <!-- Variable -->
    <xsl:variable name="title" select="/extract/name"/>
    <xsl:variable name="nbArticle" select="count(/extract/article)"/>
    <xsl:variable name="nbInpro" select="count(/extract/inproceedings)"/>
    <xsl:variable name="nbTotal" select="$nbArticle + $nbInpro"/>
    <xsl:variable name="nbCoauthor" select="count(/extract/coauthors/author)"/>

    <!-- Rendu Html -->
    <xsl:template match="/extract">
        <html>
            <head>
                <meta charset="UTF-8"/>
                <title>
                    Extrait concernant
                    <xsl:value-of select="$title"/>
                </title>
            </head>

            <body>

                <h1>
                    Extrait concernant
                    <xsl:value-of select="$title"/>
                </h1>

                <!--Les Coauteurs -->
                <h2>Coauteurs</h2>
                <div>
                    <p>
                        <xsl:value-of select="$title"/>
                        a écrit avec
                        <xsl:value-of select="$nbCoauthor"/> personne<xsl:if test="$nbCoauthor &gt; 1">s</xsl:if> :
                    </p>

                    <!-- recupération des coauteurs -->
                    <ul>
                        <xsl:for-each select="coauthors/author">
                            <xsl:sort select="." data-type="text"/>
                            <li>
                                <xsl:value-of select="."/>
                            </li>
                        </xsl:for-each>
                    </ul>

                </div>

                <!--Les Articles -->
                <h2>Articles</h2>
                <div>
                    <xsl:choose>
                        <xsl:when test="$nbArticle &gt; 0">
                            <p>
                                <xsl:value-of select="$title"/>
                                a écrit
                                <xsl:value-of select="count(article)"/>
                                article<xsl:if test="$nbArticle &gt; 1">s</xsl:if> :
                            </p>

                            <ul>
                                <xsl:for-each select="article">
                                    <xsl:sort select="year" data-type="text"/>
                                    <li>
                                        Article "<xsl:value-of select="title"/>", publié en <xsl:value-of
                                            select="year"/>,
                                        auteur<xsl:if
                                            test="count(author) &gt; 1">s
                                    </xsl:if> :
                                        <ul>
                                            <xsl:for-each select="author">
                                                <li>
                                                    <xsl:value-of select="."/>
                                                </li>
                                            </xsl:for-each>
                                        </ul>
                                    </li>
                                </xsl:for-each>
                            </ul>
                        </xsl:when>
                        <xsl:otherwise>
                            <p>
                                <xsl:value-of select="$title"/> n'a pas écrit d'article.
                            </p>
                        </xsl:otherwise>
                    </xsl:choose>
                </div>

                <!--Les InProceedings-->
                <h2>InProceeding</h2>
                <div>
                    <xsl:choose>
                        <xsl:when test="$nbInpro &gt; 0">
                            <p>
                                <xsl:value-of select="$title"/>
                                a écrit
                                <xsl:value-of select="$nbInpro"/>
                                article<xsl:if test="$nbInpro &gt; 1">s</xsl:if> de type inproceeding :
                            </p>
                            <ul>
                                <xsl:for-each select="inproceedings">
                                    <xsl:sort select="year" data-type="text"/>
                                    <li>
                                        InProceeding article "<xsl:value-of select="title"/>", publié en <xsl:value-of
                                            select="year"/>, auteur<xsl:if test="count(author) &gt; 1">s</xsl:if> :
                                        <ul>
                                            <xsl:for-each select="author">
                                                <li>
                                                    <xsl:value-of select="."/>
                                                </li>
                                            </xsl:for-each>
                                        </ul>
                                    </li>
                                </xsl:for-each>
                            </ul>
                        </xsl:when>
                        <xsl:otherwise>
                            <p>
                                <xsl:value-of select="$title"/> n'a pas écrit d'article de type inproceedings.
                            </p>
                        </xsl:otherwise>
                    </xsl:choose>
                </div>

                <!-- Statistique -->
                <h3>Statistiques sur les articles</h3>
                <ul>
                    <li>Il y a
                        <xsl:value-of select="$nbArticle"/> article<xsl:if test="$nbArticle &gt; 1">s</xsl:if>.
                    </li>
                    <li>Il y a
                        <xsl:value-of select="$nbInpro"/> article<xsl:if test="$nbInpro &gt; 1">s</xsl:if> de type
                        Inprocessing.
                    </li>
                    <li>Il y a
                        <xsl:value-of select="$nbTotal"/> article<xsl:if test="$nbTotal &gt; 1">s</xsl:if> de tous les
                        types.
                    </li>

                    <xsl:for-each select="article/year[not(preceding::year/. = .)]">
                        <xsl:sort select="."/>
                        <xsl:variable name="idYear">
                            <xsl:value-of select="."/>
                        </xsl:variable>
                        <li>En <xsl:value-of select="$idYear"/>, il y a eu
                            <xsl:value-of select="count(/extract/article[year = $idYear])"/> article<xsl:if
                                    test="count(/extract/article[year = $idYear]) &gt; 1">s</xsl:if>.
                        </li>
                    </xsl:for-each>

                    <xsl:for-each select="inproceedings/year[not(preceding::year/. = .)]">
                        <xsl:sort select="."/>
                        <xsl:variable name="idYear">
                            <xsl:value-of select="."/>
                        </xsl:variable>
                        <li>En <xsl:value-of select="."/>, il y a eu
                            <xsl:value-of select="count(/extract/inproceedings[year = $idYear])"/> article<xsl:if
                                    test="count(/extract/inproceedings[year = $idYear]) &gt; 1">s
                            </xsl:if> de type inproceedings.
                        </li>
                    </xsl:for-each>
                </ul>

            </body>
        </html>

    </xsl:template>

</xsl:stylesheet>