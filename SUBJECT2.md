# TP SAX: PARTIE II

Vous devez réaliser un programme permettant d'extraire une partie de la base de données DBLP représentée au format XML.
Le choix du langage de programmation et des technologies utilisés sont libres. La qualité du code source ne sera pas évaluée, seul le résultat le sera.
Votre programme doit, à partir d'un nom d'auteur donné en argument, extraire les informations le concernant sous la forme d'un document XML.

On souhaite extraire les "article" et les "inproceedings" concernant l'auteur, ainsi que la liste de ses coauteurs. Le document résultat devra ressembler à:

```xml
<extract>
    <name>Totor le Castor</name>
    <coauthors>
        <author>Toto</author>
        <author>Titi</author>
    </coauthors>
<article key="abcdef">
	<author>Totor le Castor</author>
	<author>Toto</author>
	<author>Titi</author>
	<title>Un article</title>
	<year>2018</year>
</article>
</extract>
```

Le document doit respecter la DTD suivante:

```dtd
<!ELEMENT extract (name, coauthors, article*, inproceedings*)>
<!ELEMENT name (#PCDATA)>
<!ELEMENT coauthors (author*)>
<!ELEMENT article (author+,title,year)*>
<!ATTLIST article key CDATA #REQUIRED>
<!ELEMENT inproceedings (author+,title,year)*>
<!ATTLIST inproceedings key CDATA #REQUIRED>
<!ELEMENT author (#PCDATA)>
<!ELEMENT title (#PCDATA)>
<!ELEMENT year (#PCDATA)>
```

Notez que le contenu des éléments "article" et "inproceedings" est un sous-ensemble des éléments d'origine.

## Critères d'évaluation

Le programme sera testé sur 4 noms d'auteurs.
Pour chaque nom, si le document XML généré est conforme à la DTD mais le contenu ne correspond pas à ce qui est attendu: 1 point.
Si le document est conforme, et contient les bonnes informations sauf ce qui concerne les coauteurs : 3 points.
Si le contenu est conforme et contient les bonnes informations): 5 points.

Pour chaque auteur, le programme sera killé automatiquement au bout de 5 minutes.

## Détails pratiques
Ce travail est à réaliser en binôme. Vous devrez remettre à l'enseignant une archive .zip ou .tar.gz ou .tgz dont le nom sera les noms de familles dex deux membres du binôme, séparés par un tiret, suivi d'un tiret et du nombre 2 (pour "tp n°2"). Les
symbôles non alphabétiques seront supprimés, les accents et cédilles supprimés. Par exemple, si votre nom de famille est "de La Tour" et celui de votre binôme "Petit-Grégoire", votre fichier s'appellera
"delatour-petitgregoire-2.tgz". 

L'archive doit contenir tous vos fichiers sources, ainsi qu'un fichier README indiquant :
- comment compiler votre programme,
- comment l'utiliser.

**ATTENTION!** le non-respect de ces consignes entraînera une pénalité de 5 points. En cas de doute, demandez à l'enseignant.


Le tout doit être envoyé par mail à l'adresse fdelorme@pm.me au plus tard la veille au soir de l'examen écrit.
