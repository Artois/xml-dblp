# TP XSLT

Vous devez réaliser un document XSLT permettant de transformer le document produit en partie II du TP sur SAX en HTML5.
Si, par exemple, le document produit est:

    <extract>
        <name>Totor le Castor</name>
        <coauthors>
            <author>Toto</author>
            <author>Titi</author>
        </coauthors>
        <article key="abcdef">
            <author>Totor le Castor</author>
            <author>Toto</author>
            <author>Titi</author>
            <title>Un article</title>
            <year>2018</year>
	   </article>
    </extract>

Le document HTML5 en sortie sera:

    <!DOCTYPE html>
    <html>
        <head>
            <meta charset="UTF-8"/>
            <title>Extrait concernant Totor le Castor</title>
        </head>
        <body>
            <h1>Extrait concernant Totor le Castor</h1>
            <h2>Coauteurs</h2>
            <div>
                <p>Totor le castor a écrit avec 2 personnes:</p>
                <ul>
                    <li>Titi</li>
                    <li>Toto</li>
                </ul>
            </div>
            <h2>Articles</h2>
            <div>
                <p>Totor le Castor a écrit 1 article:</p>
                <ul>
                    <li>
                        Article "Un article", publié en 2018, auteurs:
                        <ul>
                            <li>Totor le Castor</li>
                            <li>Toto</li>
                            <li>Titi</li>
                        </ul>
                    </li>
                </ul>
            </div>
            <h2>InProceedings</h2>
            <div>
                <p>Totor le Castor n'a pas écrit d'article de type inproceedings.</p>
            </div>
            <h3>Statistiques sur les articles</h3>
            <p>En 2018, Totor le Castor a écrit 1 article.</p>
        </body>
    </html>

## Précisions
Le document produit indiquera le nombre de coauteurs de l'auteur (attention au pluriel: "0 personne", "1 personne", "2 personnes", etc.), ainsi que les noms des coauteurs, triés par ordre alphabétique.

Puis il contiendra la liste des articles, triés de l'année la plus
ancienne à l'année la plus récente, en indiquant la liste des auteurs de
chaque article, cette fois dans l'ordre du document d'origine. De la même
manière, il contiendra la liste des in proceedings.

Enfin, on indiquera les statistiques, année par année, en précisant le
nombre d'articles (et uniquement les articles) publiés cette année-là.
À nouveau, attention aux pluriels. Si, une année, il n'y a pas eu de
publication, on l'ignore. Les statistiques seront triées par ordre
croissant (de l'année la plus ancienne à la plus récente).

## Détails pratiques
Ce travail est à réaliser en binôme. Vous devrez remettre à l'enseignant
un fichier .xsl dont le nom sera les noms de familles dex deux membres du
binôme. Les symbôles non alphabétiques seront supprimés, les accents et
cédilles supprimés. Par exemple, si votre nom de famille est "de La Tour"
et celui de votre binôme "Petit-Grégoire", votre fichier s'appellera
"delatour-petitgregoire.xsl". **ATTENTION!** le non-respect de cette
consigne entraînera une pénalité de 5 points. En cas de doute, demandez
à l'enseignant.

Le fichier doit être envoyé par mail à l'adresse fdelorme@pm.me au plus
tard la veille au soir de l'examen écrit.
